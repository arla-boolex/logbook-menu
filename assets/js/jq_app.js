var $j = jQuery.noConflict();

$j(document).ready(function() {

    $j('#checkin-date, #checkout-date').datepicker();

    $j('#checkin-time, #checkout-time').ptTimeSelect({
        'zIndex': 1000
    });

});