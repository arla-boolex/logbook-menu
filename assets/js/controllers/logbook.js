(function () {

    'use strict';

    angular.module('logbookApp')
        .controller('LogbookController', LogbookController);

    LogbookController.$inject = ['$scope', '$window', 'LogbookService'];

    function LogbookController($scope, $window, LogbookService)
    {
        var self = this;

        self.geocoder = new google.maps.Geocoder();

        self.logbooks = {};
        self.logbook = {};

        self.checkin_lat = 10.342778550438075;
        self.checkin_long = 123.94464773559571;

        self.checkout_lat = 10.31862891253175;
        self.checkout_long = 123.88748449707032;

        self.isCheckout = true;

        LogbookService.getAll()
            .then(function (response) {
                self.logbooks = response.data;
            }, function (response) {
                console.log(response);
            });

        LogbookService.getUsers()
            .success(function (response) {
                self.users = response;

                self.user_id = {id: self.users[0].id, name: self.users[0].name};
            })
            .error(function (response) {
                console.log(response);
            });

        self.addLog = function () {
            var check_out = null, checkout_lat = null, checkout_long = null;
            if (self.isCheckout)
            {
                check_out = $j('#checkout-date').val() + ' ' + $j('#checkout-time').val();
                checkout_lat = $j('#checkout-lat').val();
                checkout_long = $j('#checkout-long').val();
            }

            var data = {
                user_id: self.user_id.id,
                check_in: $j('#checkin-date').val() + ' ' + $j('#checkin-time').val(),
                checkin_lat: $j('#checkin-lat').val(),
                checkin_long: $j('#checkin-long').val(),
                check_out: check_out,
                checkout_lat: checkout_lat,
                checkout_long: checkout_long,
                zoom_level: $j('#zoom-level').val()
            };

            LogbookService.addLog(data)
                .success(function (response) {
                    $window.location.href = '/wp-admin/admin.php?page=logbook';
                })
                .error(function (response) {
                    console.log(response);
                });
        };
        
        self.getLog = function (log_id) {
            LogbookService.getLog(log_id)
                .success(function (response) {
                    self.log_id = response.id;

                    self.user_id = response.user_id;
                    self.user_login = response.user_login;

                    self.check_in = Date.parse(response.check_in);
                    self.checkin_lat = response.checkin_lat;
                    self.checkin_long = response.checkin_long;

                    self.check_out = response.check_out ? Date.parse(response.check_out) : '';
                    self.checkout_lat = response.check_out ? response.checkout_lat : self.checkout_lat;
                    self.checkout_long = response.check_out ? response.checkout_long : self.checkout_long;

                    self.zoom_level = response.zoom_level !== 0 ? parseInt(response.zoom_level) : 10;

                    initMap(self.checkin_lat, self.checkin_long, self.checkout_lat, self.checkout_long, self.zoom_level);
                })
                .error(function (response) {
                    console.log(response);
                });
        };

        self.editLog = function (log_id) {
            var check_out = null, checkout_lat = null, checkout_long = null;
            if (self.isCheckout)
            {
                check_out = $j('#checkout-date').val() + ' ' + $j('#checkout-time').val();
                checkout_lat = $j('#checkout-lat').val();
                checkout_long = $j('#checkout-long').val();
            }

            var data = {
                id: log_id,
                user_id: self.user_id,
                check_in: $j('#checkin-date').val() + ' ' + $j('#checkin-time').val(),
                checkin_lat: $j('#checkin-lat').val(),
                checkin_long: $j('#checkin-long').val(),
                check_out: check_out,
                checkout_lat: checkout_lat,
                checkout_long: checkout_long,
                zoom_level: $j('#zoom-level').val()
            };

            LogbookService.editLog(data)
                .success(function (response) {
                    $window.location.href = '/wp-admin/admin.php?page=logbook';
                })
                .error(function (response) {
                    console.log(response);
                });
        };

        // Initialize map on page load
        initMap(self.checkin_lat, self.checkin_long, self.checkout_lat, self.checkout_long, 10);

        function geocodePosition(pos, elem)
        {
            self.geocoder.geocode({
                latLng: pos
            }, function(responses) {
                if (responses && responses.length > 0)
                {
                    updateMarkerAddress(responses[0].formatted_address, elem);
                }
                else
                {
                    updateMarkerAddress('Cannot determine address at this location.', elem);
                }
            });
        }

        function updateMarkerStatus(str)
        {
            document.getElementById('markerStatus').innerHTML = str;
        }

        function updateMarkerPosition(latLng, type)
        {
            if (type === 'checkin')
            {
                $j('#checkin-lat').val(latLng.lat());
                $j('#checkin-long').val(latLng.lng());
            }
            else
            {
                $j('#checkout-lat').val(latLng.lat());
                $j('#checkout-long').val(latLng.lng());
            }
        }

        function updateMarkerAddress(str, elem)
        {
            $j('#' + elem).html(str);
        }

        function updateZoomValue(map)
        {
            $j('#zoom-level').val(map.getZoom());
        }

        function initialize(checkinLat, checkinLong, checkoutLat, checkoutLong, zoomLevel) {
            var latLngCheckin = new google.maps.LatLng(checkinLat, checkinLong);
            var latLngCheckout = new google.maps.LatLng(checkoutLat, checkoutLong);

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: zoomLevel,
                center: latLngCheckin,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            // Create marker for checkin
            var ci_marker = new google.maps.Marker({
                position: latLngCheckin,
                title: 'Check In',
                map: map,
                icon: "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
                draggable: true
            });

            // Create marker for checkout
            var co_marker = new google.maps.Marker({
                position: latLngCheckout,
                title: 'Check Out',
                map: map,
                icon: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
                draggable: true
            });

            // Update current position info of checkin
            updateMarkerPosition(latLngCheckin, 'checkin');
            geocodePosition(latLngCheckin, 'addressCheckin');

            // Update current position info of checkout
            updateMarkerPosition(latLngCheckout, 'checkout');
            geocodePosition(latLngCheckout, 'addressCheckout');

            // Get zoom level
            updateZoomValue(map);

            // Add dragging event listeners.
            /*google.maps.event.addListener(marker, 'dragstart', function() {
             updateMarkerAddress('Dragging...');
             });*/

            google.maps.event.addListener(map, 'zoom_changed', function () {
                updateZoomValue(map);
            });

            google.maps.event.addListener(ci_marker, 'drag', function() {
                //updateMarkerStatus('Dragging...');
                updateMarkerPosition(ci_marker.getPosition(), 'checkin');
            });

            google.maps.event.addListener(co_marker, 'drag', function() {
                //updateMarkerStatus('Dragging...');
                updateMarkerPosition(co_marker.getPosition(), 'checkout');
            });

            google.maps.event.addListener(ci_marker, 'dragend', function() {
                //updateMarkerStatus('Drag ended');
                geocodePosition(ci_marker.getPosition(), 'addressCheckin');
            });

            google.maps.event.addListener(co_marker, 'dragend', function() {
                //updateMarkerStatus('Drag ended');
                geocodePosition(co_marker.getPosition(), 'addressCheckout');
            });
        }

        // Initialize map
        function initMap(checkinLat, checkinLong, checkoutLat, checkoutLong, zoomLevel) {
            google.maps.event.addDomListener(window, 'load', initialize(checkinLat, checkinLong, checkoutLat, checkoutLong, zoomLevel));
        }

    }

})();