(function () {

    'use strict';

    angular.module('logbookApp')
        .factory('LogbookService', LogbookService);

    LogbookService.$inject = ['$http', '$rootScope'];

    function LogbookService($http, $rootScope)
    {
        var Service = {};

        Service.getAll = function () {
            return $http.get('/wp-json/logbook/v1/checker');
        };

        Service.getUsers = function () {
            return $http.get('/wp-json/wp/v2/users');
        };

        Service.addLog = function (data) {
            return $http.post('/wp-json/logbook/v1/checker', data);
        };

        Service.getLog = function (log_id) {
            return $http.get('/wp-json/logbook/v1/checker/' + log_id);
        };

        Service.editLog = function (data) {
            return $http.post('/wp-json/logbook/v1/checker/' + data.id, data);
        };

        return Service;
    }

})();