<?php
/**
 * @package Logbook Menu
 */
/*
Plugin Name: Logbook Menu

Description: Custom logbook menu plugin by Ingenting Inc.
Author: Ingenting Inc. Developers
Author URI: http://boolex.com
License: GPLv2 or later
Text Domain: logbook menu

Copyright 2005-2015 Ingenting Inc.
*/

add_action( 'admin_menu', 'logbook_menu' );
add_action('admin_enqueue_scripts','scripts_loader');

function logbook_menu() {
    add_menu_page( 'Logbook', 'Logbook', 'manage_options', 'logbook', 'logbook_page_loader', 'dashicons-admin-page' );
    add_submenu_page( 'logbook', 'Add Log', 'Add Log', 'manage_options', 'logbook-add', 'logbook_add_page_loader' );
}

function logbook_page_loader() {
    if ( ! current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }

    include 'views/index.php';
}

function logbook_add_page_loader() {
    if ( ! current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }

    include 'views/add.php';
}

function scripts_loader() {
    wp_register_script('gmap-api-js', 'http://maps.google.com/maps/api/js');
    wp_enqueue_script('gmap-api-js');

    wp_enqueue_script('angular-js', plugins_url('/assets/js/vendor/angular.min.js', __FILE__));
    wp_enqueue_script('lb-app-js', plugins_url('/assets/js/logbook_app.js', __FILE__));
    wp_enqueue_script('lb-cont-js', plugins_url('/assets/js/controllers/logbook.js', __FILE__));
    wp_enqueue_script('lb-serv-js', plugins_url('/assets/js/services/logbook_service.js', __FILE__));

    wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css', false);
    wp_enqueue_script('jquery-ui-datepicker');

    wp_enqueue_style('jq-tp-css', plugins_url('/assets/css/vendor/jquery.ptTimeSelect.css', __FILE__));
    wp_enqueue_script('jq-tp-js', plugins_url('/assets/js/vendor/jquery.ptTimeSelect.js', __FILE__), ['jquery']);

    wp_enqueue_script('lb-jq-app-js', plugins_url('/assets/js/jq_app.js', __FILE__), ['jquery']);

    wp_enqueue_style('lb-style-css', plugins_url('/assets/css/logbook_style.css', __FILE__));
}