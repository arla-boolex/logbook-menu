<div class="lb-container">
    <div class="lb-row">
        <div class="lb-left">

            <div class="lb-well-white">
                <table class="form-table">
                    <tbody>
                    <tr class="form-field form-required">
                        <th scope="row">
                            <label for="user"><span class="description">*</span> Worker: </label>
                            <span style="<?= $type == 'edit' ? '' : 'display: none;' ?>"><strong>{{ log.user_login }}</strong></span>

                            <select ng-model="log.user_id"
                                    ng-options="user as user.name for user in log.users track by user.id"
                                    style="<?= $type == 'add' ? '' : 'display: none;' ?>"></select>
                        </th>
                    </tr>
                    </tbody>
                </table>

                <h3>Check-in</h3>
                <table class="form-table">
                    <tbody>
                    <tr class="form-field form-required">
                        <th scope="row">
                            <label for="checkin-date"><span class="description">*</span> Date: </label>
                            <input ng-model="log.check_in_date" type="text" id="checkin-date" value="{{ log.check_in | date:'MM/dd/yyyy' }}">
                        </th>
                        <th scope="row">
                            <label for="checkin-time"><span class="description">*</span> Time: </label>
                            <input ng-model="log.check_in_time" type="text" id="checkin-time" value="{{ log.check_in | date:'h:mm a' }}">
                        </th>
                        <th scope="row">
                            <label for="checkin-lat"><span class="description">*</span> Latitude: </label>
                            <input ng-model="log.checkin_lat" type="text" id="checkin-lat" value="{{ log.checkin_lat }}" disabled>
                        </th>
                        <th scope="row">
                            <label for="checkin-long"><span class="description">*</span> Longitude: </label>
                            <input ng-model="log.checkin_long" type="text" id="checkin-long" value="{{ log.checkin_long }}" disabled>
                        </th>
                    </tr>
                    <tr class="form-field form-required">
                        <th scope="row" colspan="4">
                            <label>Address: </label>
                            <span id="addressCheckin"></span>
                        </th>
                    </tr>
                    </tbody>
                </table>

                <input type="checkbox" ng-model="log.isCheckout" class="none" id="is-checkout">
                <label for="is-checkout">Include checkout</label>

                <div ng-show="log.isCheckout">
                    <h3>Check-out</h3>
                    <table class="form-table">
                        <tbody>
                        <tr class="form-field form-required">
                            <th scope="row">
                                <label for="checkout-date"><span class="description">*</span> Date: </label>
                                <input ng-model="log.check_out_date" type="text" id="checkout-date" value="{{ log.check_out | date:'MM/dd/yyyy' }}">
                            </th>
                            <th scope="row">
                                <label for="checkout-time"><span class="description">*</span> Time: </label>
                                <input ng-model="log.check_out_time" type="text" id="checkout-time" value="{{ log.check_out | date:'h:mm a' }}">
                            </th>
                            <th scope="row">
                                <label for="checkout-lat"><span class="description">*</span> Latitude: </label>
                                <input ng-model="log.checkout_lat" type="text" id="checkout-lat" value="{{ log.checkout_lat }}" disabled>
                            </th>
                            <th scope="row">
                                <label for="checkout-long"><span class="description">*</span> Longitude: </label>
                                <input ng-model="log.checkout_long" type="text" id="checkout-long" value="{{ log.checkout_long }}" disabled>
                            </th>
                        </tr>
                        <tr class="form-field form-required">
                            <th scope="row" colspan="4">
                                <label>Address: </label>
                                <span id="addressCheckout"></span>
                            </th>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <input type="hidden" id="zoom-level">

                <hr/>
                <button type="submit" class="button button-primary"><?= $type === 'edit' ? 'Update Log' : 'Add Log'; ?></button>

            </div>

        </div>
        <div class="lb-right">

            <div class="lb-well-white">
                <h3>Choose where the worker is located.</h3>

                <div id="mapCanvas"></div>
            </div>

        </div>
    </div>
</div>