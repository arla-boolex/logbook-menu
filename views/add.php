<div ng-app="logbookApp">
    <div class="wrap" ng-controller="LogbookController as log">
        <h3>Add New Log</h3>

        <form ng-submit="log.addLog()">

            <?php
                $type = 'add';

                include '_form.php';
            ?>

        </form>
    </div>
</div>