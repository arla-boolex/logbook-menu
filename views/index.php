<div ng-app="logbookApp">
    <div class="wrap" ng-controller="LogbookController as log">

        <div class="pull-right">
            <a href="/wp-admin/admin.php?page=logbook-add" class="button button-primary">Add Log</a>
            <a href="/wp-json/logbook/v1/excel/export" class="button button-primary">Export Excel</a>
        </div>

        <h1><?= esc_html( get_admin_page_title() ); ?></h1>

        <hr/>

        <table class="wp-list-table widefat fixed striped">
            <thead>
            <tr>
                <th>User ID</th>
                <th>Name</th>
                <th>Check-in</th>
                <th>Check-out</th>
                <th>Total Time</th>
                <th>Distance</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="l in log.logbooks">
                <td>{{ l.user_id }}</td>
                <td>{{ l.display_name }}</td>
                <td>{{ l.check_in }}</td>
                <td>{{ l.check_out }}</td>
                <td>{{ l.total_time }}</td>
                <td>{{ l.distance }}</td>
                <td><a href="#TB_inline?width=1000&height=600&inlineId=lb-tickbox-content-edit" class="thickbox" ng-click="log.getLog(l.id)">Edit</a></td>
            </tr>
            </tbody>
        </table>

        <?php add_thickbox(); ?>
        <!-- Edit log with dynamic content -->
        <div id="lb-tickbox-content-edit" style="display: none;">
            <form ng-submit="log.editLog(log.log_id)">

                <?php
                    $type = 'edit';

                    include '_form.php';
                ?>

            </form>
        </div>

    </div>
</div>